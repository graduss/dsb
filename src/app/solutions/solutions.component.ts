import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { JobService, Solution, Job } from '../job.service';
import { ModalWindowService } from '../modal-window.service';

@Component({
  selector: 'app-solutions',
  templateUrl: './solutions.component.html',
  styleUrls: ['./solutions.component.scss']
})
export class SolutionsComponent implements OnInit {
  @Input() job:Job;
  @Input() selectedId:number;
  @Output() selectSolution = new EventEmitter();

  solutions:Solution[] = [];
  newSolution:Solution = null;

  constructor(
    private jobService:JobService,
    private mwService:ModalWindowService
  ) { }

  select(solution:Solution) {
    this.selectSolution.emit({solution:solution})
  }

  openWindow () {
    this.mwService.open('new-solution')
  }

  joinTo (solution:Solution) {
    this.jobService.joinToTeam(solution)
    .subscribe(() => {
      solution.joined = true;
    })
  }

  addNew () {
    this.newSolution.joined = true;
    this.solutions.push(this.newSolution);
    this.newSolution = null;
    this.mwService.close('new-solution')
  }

  ngOnInit() {
    this.newSolution = new Solution({});
    this.getSulutions();
  }

  private getSulutions ():void {
    this.jobService.getSolutionsByJobId(this.job.id)
    .subscribe(solutions => this.solutions = solutions);
  }

}

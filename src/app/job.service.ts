import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthService } from './auth.service';

const host = 'http://localhost:3000';

export class Job {
  id:number=null;
  name:string = '';
  description:string = '';
  attachments:any[] = [];
  solutionCount:number = 0;
  bestSolution:number = 0;
  reward:number =  0;
  complited:boolean = false;
  solutionId:number = null;

  constructor(data: {id?:number,name?:string,description?:string,attachments?:any[],solutionCount?:number, bestSolution?:number, reward?:number}) {
    Object.assign(this,data);
  }
}

export class Solution {
  id:number = null;
  executor:string = '';
  result:number = 0;
  attachments:string[] = [];
  joined:boolean = false;

  constructor (data:{id?:number, executor?:string, result?:number, attachments?:string[]}) {
    Object.assign(this,data);
  }
}

@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(
    private http:HttpClient,
    private authService:AuthService
  ) {}

  getJobs():Observable<Job[]> {
    return this.http.get<{jobs:any[]}>(`${host}/api/jobs`)
    .pipe(
      map((data) => data.jobs.map((item) => new Job(item)))
    )
  }

  getNews():Observable<object> {
    return this.http.get<{jobs:any[]}>(`${host}/api/news`)
  }

  getJobById (id:number):Observable<Job> {
    return this.http.get(`${host}/api/job/${id}`)
    .pipe(
      map((data) => new Job(data))
    )
  }

  getSolutionsByJobId (jobId:number):Observable<Solution[]> {
    return this.http.get<{solutions:any[]}>(`${host}/api/${jobId}/solutions`)
    .pipe(
      map(data => data.solutions.map(item => new Solution(item)))
    )
  }

  joinToTeam(solution:Solution):Observable<object> {
    return this.http.post(`${host}/api/solution/${solution.id}/join`, { id: this.authService.currentUserId})
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BoardComponent } from './board/board.component';
import { JobDetailsComponent } from "./job-details/job-details.component";

const routes: Routes = [
  { path: '', component: BoardComponent },
  { path: 'job/:id', component: JobDetailsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

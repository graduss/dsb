import { Component, OnInit } from '@angular/core';
import { JobService, Job } from "../job.service";;

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  jobs:Job[] = [];
  news: object = null;

  constructor(private jobService:JobService) { }

  ngOnInit() {
    this.getJobs();
    this.getNews();
  }

  private getJobs () {
    this.jobService.getJobs()
    .subscribe((list) => this.jobs = list)
  }

  private getNews () {
    this.jobService.getNews()
    .subscribe((data) => this.news = data)
  }

}

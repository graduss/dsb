import { Component, OnInit, ElementRef, Input, OnDestroy } from '@angular/core';
import { ModalWindowService } from '../modal-window.service';

@Component({
  selector: 'app-modal-window',
  templateUrl: './modal-window.component.html',
  styleUrls: ['./modal-window.component.scss']
})
export class ModalWindowComponent implements OnInit, OnDestroy {
  @Input() id:string
  private element:HTMLElement;
  isShow:boolean = false;

  constructor(
    private mwService:ModalWindowService,
    private el:ElementRef
  ) {
    this.element = this.el.nativeElement;
  }

  ngOnInit() {
    document.body.appendChild(this.element);
    this.mwService.add(this);
  }

  ngOnDestroy () {
    this.mwService.remove(this.id);
    this.element.remove();
  }

  open(): void {
    this.isShow = true;
  }

  close(): void {
    this.isShow = false;
  }

}

import { Component, OnInit, Input } from '@angular/core';
import { Job } from "../job.service";

@Component({
  selector: 'app-job-thumbnail',
  templateUrl: './job-thumbnail.component.html',
  styleUrls: ['./job-thumbnail.component.scss']
})
export class JobThumbnailComponent implements OnInit {
  @Input() job:Job

  constructor() { }

  ngOnInit() {
  }

}

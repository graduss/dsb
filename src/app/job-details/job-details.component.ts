import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobService, Job, Solution } from '../job.service';

@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.scss']
})
export class JobDetailsComponent implements OnInit {
  job:Job = null;

  constructor(
    private route:ActivatedRoute,
    private jobService:JobService
  ) { }

  selectSolution ({solution}) {
    this.job.solutionId = solution.id;
  }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.getJob(id);
  }

  private getJob(id:number) {
    this.jobService.getJobById(id)
    .subscribe((job) => this.job = job);
  }

}

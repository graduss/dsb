import { Injectable } from '@angular/core';
import { ModalWindowComponent } from "./modal-window/modal-window.component";

@Injectable({
  providedIn: 'root'
})
export class ModalWindowService {

  private modals:ModalWindowComponent[] = [];

  constructor() { }

  add (modal:ModalWindowComponent):void {
    this.modals.push(modal)
  }

  remove(id: string) {
    this.modals = this.modals.filter(x => x.id !== id);
  }

  open(id: string) {
    const modal: ModalWindowComponent = this.modals.filter(x => x.id === id)[0];
    modal.open();
  }

  close (id:string) {
    const modal: ModalWindowComponent = this.modals.filter(x => x.id === id)[0];
    modal.close();
  }
}

const express = require("express");
const faker = require('faker');

const app = express();

// app.get('/api/user', function(req, res) {
//   res.json({
//     name: faker.name.findName(),
//     email: faker.internet.email(),
//     address: faker.address.streetAddress(),
//     bio: faker.lorem.sentence(),
//     image: faker.image.avatar()
//   });
// });

class Job {
  constructor(data) {
    Object.assign(this,{
      id: faker.random.number(),
      name: faker.random.word(),
      description: faker.lorem.words(),
      attachments: [faker.system.fileName(),faker.system.fileName(),faker.system.fileName()],
      solutionCount: faker.random.number() % 1000,
      bestSolution: Math.random(),
      reward: faker.random.number() % 1000,
      complited: false,
      solutionId: null
    },data);
  }
}

class Solution {
  constructor (data) {
    Object.assign(this,{
      id: faker.random.number(),
      executor: `${faker.name.firstName()} ${faker.name.lastName()}`,
      result: Math.random(),
      attachments: [faker.system.fileName(),faker.system.fileName(),faker.system.fileName()],
      joined: false
    },data)
  }
}

app.get('/api/jobs', function (req,res) {
  res.header('Access-Control-Allow-Origin' , '*' );
  res.json({
    jobs: Array.from(new Array(10)).map(() => new Job)
  })
});

app.get('/api/job/:id', function (req,res) {
  let id = +req.param('id');
  res.header('Access-Control-Allow-Origin' , '*' );
  res.json(new Job({id}));
});

app.get('/api/news', function (req,res) {
  let id = +req.param('id');
  res.header('Access-Control-Allow-Origin' , '*' );
  res.json({
    date: new Date,
    command: 'bestofBest',
    value: 1000
  })
});

app.get('/api/:jobId/solutions', (req,res) => {
  let jobId = +req.param('id');
  res.header('Access-Control-Allow-Origin' , '*' );
  res.json({
    solutions: Array.from(new Array(10)).map(() => new Solution)
  });
})

app.post('/api/solution/:id/join', (req,res) => {
  res.header('Access-Control-Allow-Origin' , '*' );
  res.json({});
});



app.listen(3000, () => {
 console.log("Server running on port 3000");
});
